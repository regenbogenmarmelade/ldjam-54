using Godot;
using System;

public class Robot : Sprite
{
    public override void _Ready() {
        GetNode<Label>("Label").Visible = false; 
    }


    private void OnMouseEntered() {
        GetNode<Label>("Label").Visible = true; 
    }
    
    private void OnMouseExited() {
        GetNode<Label>("Label").Visible = false; 
    }
}
