using Godot;
using System;

public class explosion : Particles2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    private ulong timeCreated;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        timeCreated = Time.GetTicksMsec();
    }

    

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if ( Time.GetTicksMsec() - timeCreated > Lifetime * 1000 * 5) {
            QueueFree();
        }
    }
}
