using Godot;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public class Cargohold : Node2D
{

    [Export]
    private int MaximumCargoCount = 15;

    [Export]
    private int MaxVelocitySecuredCargo = 50;

    [Export]
    private Vector2 CargoShipPosition = new Vector2(892, 906);

    [Export]
    private int ShipAcceleration = 100;

    [Export]
    private PackedScene Explosion;



    // list of cargo that is in the storage an is available to be loaded
    private List<Cargo> AvailableCargo = new List<Cargo>();
    private int currentCargoIndex = 0;
    private int RemainingCargo => AvailableCargo.Count(cargo => cargo.CargoState == CargoState.inCargoholdStorage);

    // indicates id the start button has been pressed
    private bool shipStarted = false;

    private Dialog DropDialog => GetNode<Dialog>("UI/DropDialog");
    private Dialog StartShipDialog => GetNode<Dialog>("UI/StartShipDialog");
    private Node2D Ships => GetNode<Node2D>("Ship");
    private Node2D Hooks => GetNode<Node2D>("Hooks");
    private Node2D Effects => GetNode<Node2D>("Effects");
    private Node2D CargoHold => GetNode<Node2D>("CargoHold");
    private Area2D MisseedZone => GetNode<Area2D>("MissedZone");
    private Label LabelTime => GetNode<Label>("%LabelLoadingTime");
    private Timer SpawnTimer => GetNode<Timer>("%SpawnTimer");
    private AnimationPlayer AniPlayer => GetNode<AnimationPlayer>("%AnimationPlayer");
    private TextureButton StartButton => GetNode<TextureButton>("%StartButton");

    private ulong StartTime;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GD.Randomize();

        //initialize available cargo
        GameState game_state = GetNode<GameState>("/root/GameState");
        List<Cargo> roll_table = new List<Cargo>(game_state.AllCargo);
        while (AvailableCargo.Count < MaximumCargoCount && roll_table.Count > 0)
        {
            var random_cargo_index = (int)(GD.Randi() % roll_table.Count);
            var cargo = roll_table[random_cargo_index];
            if (cargo.CargoState == CargoState.deliverdSuccessfully)
            {
                // skip cargo which was delivered
                continue;
            }

            cargo.CargoState = CargoState.inCargoholdStorage;
            AvailableCargo.Add(cargo);
            MoveToCargoHold(cargo);
            roll_table.Remove(cargo);
        }

        // spawn Ship
        var packed_scene = (PackedScene)GD.Load("res://cargohold/ships/ship01.tscn");
        var ship = packed_scene.Instance<CargoShip>();
        ship.Position = CargoShipPosition;
        ship.Name = "CargoShip";
        ship.Mode = RigidBody2D.ModeEnum.Character;
        Ships.AddChild(ship);

        // update UI
        UpdateRemainingCargoLabel();
        OnSpawnTimerTimeout();

        // configure Dialogs
        if (game_state.FirstRound)
        {
            ship.Connect("body_entered", this, nameof(ShowStartShipDialog));
            StartButton.Hide();
            StartShipDialog.Connect("DialogClosed", this, nameof(ShowStartButton));

        }
        else
        {
            ShowStartButton();
            DropDialog.AlreadyShown = true;
            StartShipDialog.AlreadyShown = true;
        }

        DropDialog.Hide();

        // hide StartButton and connect to dialog 

        game_state.FirstRound = false;
        StartTime = Time.GetTicksMsec();

    }
    private void ShowStartButton()
    {
        StartButton.Show();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (!shipStarted)
        {
            ulong end_time = Time.GetTicksMsec();
            ulong time_spend = end_time - StartTime;
            ulong mseconds = time_spend % 1000;
            ulong seconds = time_spend / 1000;

            LabelTime.Text = seconds + "." + mseconds;

        }
    }

    public void ShowStartShipDialog(Node body)
    {
        if (!(body is Cargo))
        {
            return;
        }

        StartShipDialog.ShowOnce(0.5f);
    }

    public void ShowDropDialog(Node body)
    {
        if (!(body is Cargo))
        {
            return;
        }

        DropDialog.ShowOnce(2);
    }

    public void OnSpawnTimerTimeout()
    {
        AniPlayer.Play("BeamFadeIn");
    }


    public void UpdateRemainingCargoLabel()
    {
        Label remaingin_items = GetNode<Label>("%LabelRemainingItems");
        remaingin_items.Text = RemainingCargo.ToString();
    }

    public void SpawnHook()
    {
        var packed_scene = (PackedScene)GD.Load("res://cargohold/Hook.tscn");
        Vector2 spawn_point = GetNode<Node2D>("%CargoSpawnPoint").GlobalPosition;
        var hook = packed_scene.Instance<Hook>();
        hook.Spawn = spawn_point;

        Hooks.AddChild(hook);
        hook.Connect("CargoDetached", this, "OnHookDetachedCargo");


        // if no cargo availble dont spawn anything
        if (RemainingCargo == 0)
        {
            return;
        }

        // search for spwanable cargo in cargoHold and attach it to Hook
        for (var i = 0; i < AvailableCargo.Count; i++)
        {
            Cargo cargo = AvailableCargo[currentCargoIndex];
            if (cargo.CargoState == CargoState.inCargoholdStorage)
            {
                cargo.CargoState = CargoState.inPickupPlace;
                // cargo.GlobalPosition = spawn_point;
                hook.AttachCargo(cargo);
                break;
            }
            currentCargoIndex = (currentCargoIndex + 1) % AvailableCargo.Count;
        }
        UpdateRemainingCargoLabel();
    }

    public void OnHookDetachedCargo(Cargo cargo)
    {
        Node parent = cargo.GetParentOrNull<Node>();
        if (parent != null)
        {
            parent.RemoveChild(cargo);
        }

        // after simulation Frame add to right collection
        CargoHold.CallDeferred("add_child", cargo);
        // CargoHold.AddChild(cargo);
        cargo.MoveOutOfTheWindows();
        cargo.CargoState = CargoState.inCargoholdStorage;

        UpdateRemainingCargoLabel();
    }


    // Handle cargo that missed the space cargo freighter
    public void OnMissedZoneBodyEntered(object body)
    {
        if (body is Cargo cargo)
        {

            // la explosion
            if (Explosion != null)
            {
                Particles2D explosion = Explosion.Instance<Particles2D>();
                Effects.AddChild(explosion);
                explosion.GlobalPosition = cargo.GlobalPosition;
                explosion.Emitting = true;
                var Sign = GetNode<Control>("%SignDaysWithoutAccident");
                if (Sign.Visible)
                {
                    var player = Sign.GetNode<AnimationPlayer>("AnimationPlayer");
                    player.Play("FlyIn");
                }
            }


            // move cargo out of screen
            cargo.CallDeferred("MoveOutOfTheWindows");
            cargo.CargoState = CargoState.inMissedArea;
        }
    }

    public void CalculateResults()
    {
        foreach (Cargo cargo in AvailableCargo)
        {
            if (cargo.CargoState == CargoState.hasBeenRelasedFromHook)
            {
                if (cargo.LinearVelocity.LengthSquared() < MaxVelocitySecuredCargo)
                {
                    cargo.CargoState = CargoState.isLoadedOnCargoShip;
                }
                else
                {
                    cargo.CargoState = CargoState.inMissedArea;
                }
            }

        }

        var cargoLoaded = AvailableCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip);
        GD.Print("Loaded Cargo:", cargoLoaded);

        ulong end_time = Time.GetTicksMsec();
        ulong time_spend = end_time - StartTime;
        ulong mseconds = time_spend % 1000;
        ulong seconds = time_spend / 1000;


        GD.Print("Start Ship, time spend: ", seconds, ".", mseconds);
        GameState game_state = GetNode<GameState>("/root/GameState");
        game_state.CargoLoadingTimeInMs += time_spend;
    }


    public void StartShip()
    {
        Timer timer = GetNode<Timer>("%SpawnTimer");
        timer.Stop();

        CargoShip ship = GetNode<CargoShip>("Ship/CargoShip");
        if (!ship.Launched)
        {
            shipStarted = true;

            CalculateResults();
            AniPlayer.Play("StartShip");
            ship.Mode = RigidBody2D.ModeEnum.Character;
            ship.Launched = true;
            ship.Launch();
            ship.AddCentralForce(new Vector2(-ShipAcceleration, 0));
        }
    }

    public void StartNextScene()
    {
        foreach (Cargo cargo in AvailableCargo)
        {
            Node parent = cargo.GetParentOrNull<Node>();
            if (parent != null)
            {
                parent.RemoveChild(cargo);
            }
        }

        GetTree().ChangeScene("res://startSpaceShip/MiniGameStartSpaceShip.tscn");
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);
        // TODO Rebind Key ?
        if (@event.IsActionPressed("ui_up"))
        {
            StartShip();
        }
        if (@event.IsActionPressed("ui_select"))
        {

            foreach (Cargo cargo in AvailableCargo)
            {
                if (cargo.IsDroppable
                    && !shipStarted
                    && cargo.CargoState == CargoState.isOnHook)
                {
                    DropDialog.CancelDelayedShow();

                    cargo.CargoState = CargoState.hasBeenRelasedFromHook;

                    Hook h = cargo.GetParent<Hook>();
                    h.AnimatedSprite.Animation = "open";
                    h.OpenHookSound.Play();
                    h.RemoveChild(cargo);
                    CargoHold.AddChild(cargo);



                    cargo.Mode = RigidBody2D.ModeEnum.Kinematic;
                    cargo.Position = h.Position - CargoHold.Position;
                    cargo.LinearVelocity = new Vector2(0, 100);
                    cargo.GravityScale = cargo.CustomGravity;
                    cargo.Mode = RigidBody2D.ModeEnum.Rigid;

                    OnSpawnTimerTimeout();
                    SpawnTimer.Start();

                }
            }
        }
    }

    private void MoveToCargoHold(Cargo cargo)
    {
        Node parent = cargo.GetParentOrNull<Node>();
        if (parent != null)
        {
            parent.RemoveChild(cargo);
        }
        CargoHold.AddChild(cargo);
        cargo.MoveOutOfTheWindows();
    }


}


