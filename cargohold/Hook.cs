using System;
using Godot;

public class Hook : RigidBody2D
{
    public Vector2 Spawn;
    public float Width = 400;
    public AnimationPlayer AnimationPlayer => GetNode<AnimationPlayer>("AnimationPlayer");
    public AudioStreamPlayer2D OpenHookSound => GetNode<AudioStreamPlayer2D>("OpenHookSound");

    public AnimatedSprite AnimatedSprite => GetNode<AnimatedSprite>("AnimatedSprite");

    [Signal]
    public delegate void CargoDetached(Cargo cargo);

    public override void _Ready()
    {
        Visible = false;

        Mode = ModeEnum.Kinematic;
        Position = Spawn;
        Mode = ModeEnum.Character;

        Visible = true;
        AnimatedSprite.Animation = "close";
    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {
        var transform = state.Transform;

        if (transform.origin.x < -Width)
        {
            DetachCargo();

            if (!AnimationPlayer.IsPlaying())
            {
                AnimationPlayer.Play("Destroy");
            }

            return;
        }

        state.LinearVelocity = 250 * Vector2.Left;
    }

    public void AttachCargo(Cargo cargo)
    {
        Node parent = cargo.GetParent();
        if (parent != null)
        {
            parent.RemoveChild(cargo);
        }
        AddChild(cargo);
        // postion relative to us 0,0
        cargo.Position = new Vector2(0,0);
        cargo.Mode = RigidBody2D.ModeEnum.Rigid;
        cargo.CargoState = CargoState.isOnHook;
        cargo.Visible = true;
        cargo.Sleeping = false;

        Vector2 new_scale = new Vector2(cargo.CustomScale,cargo.CustomScale);

        // set scaling
        cargo.Scale = new_scale;
        cargo.GetNode<Node2D>("CollisionPolygon2D").Scale = new_scale;
        cargo.GetNode<Node2D>("Sprite").Scale = new_scale;

        // connect movement
        var joint = new PinJoint2D();
        joint.NodeA = GetPath();
        joint.NodeB = cargo.GetPath();
        AddChild(joint);


    }

    public void DetachCargo()
    {
        foreach (var node in GetChildren())
        {
            if (node is Cargo cargo)
            {
                if (cargo.CargoState == CargoState.isOnHook)
                {
                    EmitSignal("CargoDetached", cargo);
                }
            }
        }
    }
}
