using Godot;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

public class CargoShip : RigidBody2D
{
    private AudioStreamPlayer2D LaunchSound => GetNode<AudioStreamPlayer2D>("LaunchSound");
    private AudioStreamPlayer2D ThrusterLoopSound => GetNode<AudioStreamPlayer2D>("ThrusterLoopSound");

    private Particles2D ExaustEffect => GetNode<Particles2D>("ExaustEffect");


    public bool Launched = false;
    //    public bool isStaring = false;
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    // [Export]
    // private Texture ShipTexture { 
    //     get { return GetNode<Sprite>("ShipSprite").Texture ; }
    //     set { GetNode<Sprite>("ShipSprite").Texture = value; }
    // }
    // [Export]
    // private Shape ShipCollision { get; set; }

    // [Export]
    // private Shape CargoCollision { get; set; }


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // Image image = GetNode<Sprite>("ShipSprite").Texture.GetData();
        // CollisionPolygon2D collision =  GetNode<CollisionPolygon2D>("ShipCollisionPoly");
        // BitMap map = new BitMap();

        // map.CreateFromImageAlpha(image);

        // var polys = map.OpaqueToPolygons(new Rect2(new Vector2(),image.GetSize() ), 2 ).Cast<Godot.Vector2>().ToArray();
        // collision.Polygon = polys;

    }


    public float usedCargoSpace(List<Cargo> cargoList)
    {
        float result = 0F;

        Area2D collision = GetNode<Area2D>("ShipCollisionPoly");
        // collision.area


        return result;
    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {

        if (Launched)
        {
            Vector2 v = state.LinearVelocity;
            v.x += AppliedForce.x * state.Step;
            v.y = 0;
            state.LinearVelocity = v;
            ExaustEffect.Emitting = true;
        }
        else
        {
            state.LinearVelocity = new Vector2(0,0);
            ExaustEffect.Emitting = false;
        }
        // GD.Print("Volocity:", state.LinearVelocity );
        // GD.Print("Force:", AppliedForce );
    }

    public void Launch()
    {
        LaunchSound.Play();
        ThrusterLoopSound.Play();
    }
}
