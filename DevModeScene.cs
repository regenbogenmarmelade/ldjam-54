using Godot;
using System;

public class DevModeScene : CenterContainer
{
    private void StartIntro()
    {
        GetTree().ChangeScene("res://intro/Intro.tscn");
    }

    private void StartCargohold()
    {
        GetTree().ChangeScene("res://cargohold/Cargohold.tscn");
    }

    private void StartSpaceShip()
    {
        GetTree().ChangeScene("res://startSpaceShip/MiniGameStartSpaceShip.tscn");
    }

    private void StartDocking()
    {
        GetTree().ChangeScene("res://docking/DockingScene.tscn");
    }
    
    private void DeleteSettings()
    {
        GD.Print("DeleteSettings");
        UserSettings.Delete();
    }
}
