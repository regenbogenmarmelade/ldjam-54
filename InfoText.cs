using Godot;
using System;

public class InfoText : Control {

    [Export] private String text = "Some Text"; 
    private RichTextLabel label => GetNode<RichTextLabel>("Label");

    private Tween tweenNew; 

    private bool infoTextStarted = false; 

    [Export] public float textSpeed; 
    
    
    public override void _Ready()
    {
        label.Text = text;
        label.Visible = false;
        Visible = false; 
        
        GetNode<Timer>("Timer").Start();
    }
    
    private void animationFinished()
    {
        if (infoTextStarted) {
            this.Visible = false; 
        }
        
        if (!infoTextStarted) {
            typeText();
            infoTextStarted = true; 
            GetNode<Timer>("CloseTimer").Start();
        }
    }

    private void typeText() {
        label.Visible = true; 
        label.PercentVisible = 0.0f;

        tweenNew = new Tween(); 
        label.AddChild(tweenNew);
        var index = tweenNew.GetIndex();
        tweenNew.InterpolateProperty(label, "percent_visible", 0, 1, textSpeed * label.Text.Length);
        tweenNew.Start();
        tweenNew.Connect("tween_all_completed", this, nameof(onTweenFinish));
    }

    private void onTimerTimeout() {
        startMessage();
    }

    private void onTimerTimeoutClose() {
        label.Visible = false;
        GetNode<AnimatedSprite>("AnimatedSprite").Frame = 7;
        GetNode<AnimatedSprite>("AnimatedSprite").Play("default");
        
    }

    public void startMessage() {
        Visible = true;
        infoTextStarted = false; 
        GetNode<AnimatedSprite>("AnimatedSprite").Frame = 0;
        GetNode<AnimatedSprite>("AnimatedSprite").Play("backwards");
    }

    public void setNewText(String otherText) {
        label.Text = otherText;
    }
    
    private void onTweenFinish() {
        tweenNew.QueueFree();
    }
    
    public void resetMessage() {
        Visible = false;
        label.Visible = false; 
        label.PercentVisible = 0.0f;
        infoTextStarted = true; 
        GetNode<AnimatedSprite>("AnimatedSprite").Frame = 0;
        if(tweenNew != null) {
             tweenNew.QueueFree();
           }
       
    }
    
}
