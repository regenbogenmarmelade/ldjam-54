using System;

public enum CargoType{
    Battery,
    Bike,
    Cabbage,
    Cookie,
    Fragile,
    Nut,
    Robot,
    SolarPanel,
    TrojanHorse,
    BoxUp,

    // has always to be last item
    MaxCargoType,
}