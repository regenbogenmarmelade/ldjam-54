using Godot;


public enum CargoState
{
    inCargoholdStorage,
    inPickupPlace,
    isOnHook,
    hasBeenRelasedFromHook,
    isLoadedOnCargoShip,
    inMissedArea,
    lostDuringStart,
    lostDuringTransport,
    deliverdSuccessfully,
}


public class Cargo : RigidBody2D
{
    // true, if cargo is inside the Drop Area to be droped on cargo ship
    public bool IsDroppable = false;
    public bool IsOnHook => GetParent() is Hook;

    public CargoState CargoState = CargoState.inCargoholdStorage;

    [Export] public CargoType CargoType;

    [Export] public float CustomGravity = 1;

    [Export] public float CustomScale = 1;

    public AudioStreamPlayer2D CollisionSound => GetNode<AudioStreamPlayer2D>("CollisionSound");

    public override void _Ready()
    {
        base._Ready();
        Scale = new Vector2(CustomScale,CustomScale );
    }

    private void PlayCollisionSound(object body)
    {
        if (CollisionSound.Playing)
        {
            return;
        }

        CollisionSound.Play();
    }
    public void MoveOutOfTheWindows()
    {
        Mode = RigidBody2D.ModeEnum.Static;
        Sleeping = true;
        Visible = false;
        GlobalPosition = new Vector2(-1000, -1000);
        LinearVelocity = new Vector2(0, 0);
    }

}
