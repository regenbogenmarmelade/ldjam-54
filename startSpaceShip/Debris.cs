using Godot;
using System;

public class Debris : Node2D
{
    [Export] public float speed;

    private bool stopMoving = false;
    private AnimationPlayer animationPlayer;

    private Node2D realDebris => GetParent<Node2D>();
    private AudioStreamPlayer2D PlaneLoopSound => GetNode<AudioStreamPlayer2D>("PlaneLoopSound");

    public override void _Ready()
    {
        animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
        var realDebris = this.GetParent<Node2D>();
        //GD.Print(realDebris.Name);
        this.RemoveChild(animationPlayer);
        realDebris.CallDeferred("add_child", animationPlayer);
        
        GD.Randomize();
        PlaneLoopSound.Play(GD.Randf());
        PlaneLoopSound.PitchScale = 0.5f + GD.Randf();
    }
    
    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
      public override void _Process(float delta) {
          if (!stopMoving) {
              realDebris.Position += Transform.x * speed * delta;
          }
      }
    
      public void stopAndSize() {
          stopMoving = true;

          //var animation = animationPlayer.GetNode<Animation>("MoveAndSize"); 
          var animation = (Animation)animationPlayer.GetAnimation("MoveAndSize").Duplicate();
          //GD.Print(animationPlayer);

          animationPlayer.AddAnimation("MoveAndSize", animation); 
          
          var idx = animation.FindTrack(".:position:x");
          var idy = animation.FindTrack(".:position:y");
          var scaleX = animation.FindTrack(".:scale:x");
          var scaleY = animation.FindTrack(".:scale:y");
    
          int xValue;
          int yValue;
          
          
          var signX = Mathf.Sign(GetParent<Node2D>().GlobalPosition.x - 960);
          xValue = signX * (int)Mathf.Pow(Mathf.Abs(GetParent<Node2D>().GlobalPosition.x - 960), 1.3f);


          var signY = Mathf.Sign(GetParent<Node2D>().GlobalPosition.x - 540);
          yValue = signY * (int)Mathf.Pow(Mathf.Abs(GetParent<Node2D>().GlobalPosition.y - 540), 1.3f); 
          
          
          animation.BezierTrackSetKeyValue(idx,0, GetParent<Node2D>().GlobalPosition.x);
          animation.BezierTrackSetKeyValue(idx,1,GetParent<Node2D>().GlobalPosition.x + xValue);
          animation.TrackSetKeyTime(idx, 1, 1);
          
          animation.BezierTrackSetKeyValue(idy,0,GetParent<Node2D>().GlobalPosition.y);
          animation.BezierTrackSetKeyValue(idy,1,GetParent<Node2D>().GlobalPosition.y - yValue);
          animation.TrackSetKeyTime(idy, 1, 1);
          
          animation.BezierTrackSetKeyValue(scaleX,0, 1);
          animation.BezierTrackSetKeyValue(scaleX,1,4);
          animation.TrackSetKeyTime(scaleX, 1, 1);
          
          animation.BezierTrackSetKeyValue(scaleY,0,1);
          animation.BezierTrackSetKeyValue(scaleY,1,4);
          animation.TrackSetKeyTime(scaleY, 1, 1);
    
    
          // GD.Print("StartPosition: ");
          // GD.Print(this);
          // GD.Print(GetParent<Node2D>().GlobalPosition.x + "," + GetParent<Node2D>().GlobalPosition.y);
          
          animationPlayer.Play("MoveAndSize");
      }
      
      private void _on_AnimationPlayer_animation_finished(String anim_name)
      {
          if ("MoveAndSize".Equals(anim_name)) {
              // GD.Print("EndPosition: ");
              // GD.Print(this);
              // GD.Print(this.GetParent<Node2D>().GlobalPosition.x + "," + GetParent<Node2D>().GlobalPosition.y);
          }
      }
}
