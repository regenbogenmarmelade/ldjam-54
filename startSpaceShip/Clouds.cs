using Godot;
using System;

public class Clouds : Node2D {
    private Sprite cloud1 => GetNode<Sprite>("Cloud1"); 
    private Sprite cloud2 => GetNode<Sprite>("Cloud2"); 
    private Sprite cloud3 => GetNode<Sprite>("Cloud3"); 
    private Sprite cloud4 => GetNode<Sprite>("Cloud4"); 
    
    
    public override void _Process(float delta) {

        cloud1.Position = new Vector2(cloud1.GlobalPosition.x - 50f * delta, cloud1.GlobalPosition.y); 
        cloud2.Position = new Vector2(cloud2.GlobalPosition.x - 70f * delta, cloud2.GlobalPosition.y); 
        cloud3.Position = new Vector2(cloud3.GlobalPosition.x - 80f * delta, cloud3.GlobalPosition.y); 
        cloud4.Position = new Vector2(cloud4.GlobalPosition.x - 60f * delta, cloud4.GlobalPosition.y);


        if (cloud1.Position.x < -400) {
            cloud1.Position = new Vector2(2200, cloud1.GlobalPosition.y);
        }
        if (cloud2.Position.x < -400) {
            cloud2.Position = new Vector2(2200, cloud2.GlobalPosition.y);
        }
        if (cloud3.Position.x < -400) {
            cloud3.Position = new Vector2(2200, cloud3.GlobalPosition.y);
        }
        if (cloud4.Position.x < -400) {
            cloud4.Position = new Vector2(2200, cloud4.GlobalPosition.y);
        }
    }

}
