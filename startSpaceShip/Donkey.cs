using Godot;
using System;

public class Donkey : Node2D {
    private float rotationSpeed = 2;
    public override void _Ready()
    {
        
    }

  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta) {
      Rotation += rotationSpeed * delta;
  }
}
