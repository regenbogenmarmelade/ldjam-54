using Godot;
using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

public class MiniGameStartSpaceShip : Node2D {

    [Export] public String sceneName;
    private GameState gameState => GetNode<GameState>("/root/GameState");
    
    private bool pressedOnce = false;
    private bool enableCollision = false;

    private Timer timer => GetNode<Timer>("Timer");
    public AudioStreamPlayer2D LaunchSound => GetNode<AudioStreamPlayer2D>("LaunchSound");
    public AudioStreamPlayer2D CrashSound => GetNode<AudioStreamPlayer2D>("CrashSound");
    public AudioStreamPlayer2D AlarmSound => GetNode<AudioStreamPlayer2D>("AlarmSound");


    private SpaceShip spaceShip => GetNode<SpaceShip>("SpaceShip");

    private Node2D testDebris => GetNode<Node2D>("TestDebris");

    private ulong StartTime;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GetNode<Sprite>("Lines3").Visible = false;
        GetNode<Sprite>("Lines4").Visible = false;
        GetNode<Sprite>("Crack").Visible = gameState.collideWithDebrisLevel2;
        GetNode<Sprite>("Bg").Visible = true; 
        GetNode<Sprite>("Bg").Modulate = new Color(1,1,1,1);
        if ("Earth".Equals(sceneName)) {
            GetNode<ColorRect>("ColorRect").Modulate = new Color(1,1,1,1); 
            GetNode<Sprite>("Ship").GetNode<AnimationPlayer>("AnimationPlayer").Play("ShipShake");
        }

        //GetNode<ColorRect>("ColorRect").Color = new Color(209, 243, 255, 255); 
      
        timer.WaitTime = 0.7f;
        
        // if someone started the scene from dev menu, just get all cargo and flag them as loaded
        if (gameState.AllCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip) == 0) 
        {
            foreach (var cargo in gameState.AllCargo)
            {
                cargo.CargoState = CargoState.isLoadedOnCargoShip;
            };
        }

        StartTime = Time.GetTicksMsec();
    }
    
    public override void _Process(float delta) {
        if (GetNode<Area2D>("Target").GetOverlappingAreas().Count > 1) {
            changeColorRed();
        }
        else {
            changeColorBlue();
        }
    }


    public override void _UnhandledInput(InputEvent @event)
    {
        if (@event is InputEventKey eventKey && !pressedOnce)
        {
            if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Space)
            {
                //GD.Print("Debug");
                pressedOnce = true;
                timer.Start();

                var listOfDebris = GetTree().GetNodesInGroup("Debris");

                foreach (var obj in listOfDebris)
                {
                    Node2D debris = (Node2D)obj;
                    debris.GetNode<Debris>("Debris").stopAndSize();
                }

                GetNode<InfoText>("InfoText").resetMessage(); 

                GetNode<AnimationPlayer>("ShipAnimationPlayer").Play("Speed");
                LaunchSound.Play();
            }
        }
    }

    private void CollideWithDebris(object area)
    {
        if (enableCollision)
        {
            GD.Print("Test: Collide! : " + area);
            GetNode<Sprite>("Ship").GetNode<AnimationPlayer>("AnimationPlayer").Play("Collide");
            CrashSound.Play();
            AlarmSound.Play();
            GetNode<InfoText>("InfoText").setNewText("Hey you collided!");
            GetNode<InfoText>("InfoText").startMessage();
            gameState.collideWithDebrisLevel2 = true; 
            GetNode<Sprite>("Crack").Visible = true;

            var ci = gameState.AllCargo.FindIndex(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip);
            if (ci > -1)
            {
                gameState.AllCargo[ci].CargoState = CargoState.lostDuringStart;
            }
        }
    }

    private void onTimerTimeout()
    {
        enableCollision = true;
        if (spaceShip.GetOverlappingAreas().Count > 1)
        {
            CollideWithDebris(null);
        }
    }

    private void reachSpace(String anim_name)
    {
        if ("Speed".Equals(anim_name))
        {
            fadeOutDebris();
        }
    }

    private void fadeOutDebris()
    {
        var listOfDebris = GetTree().GetNodesInGroup("Debris");
        foreach (var obj in listOfDebris)
        {
            Node2D debris = (Node2D)obj;
            var tween = new Tween();
            debris.AddChild(tween);
            tween.InterpolateProperty(debris, "modulate:a", 1, 0, 0.6f);
            tween.SetActive(true);
            tween.Start();
        }
    }


    private void reachEndArea(object area) {
        
        var list = new ArrayList(); 
        var listOfDebris = GetTree().GetNodesInGroup("Debris");
        foreach (var obj in listOfDebris) {
            Node2D debris = (Node2D)obj;
            var area2D = debris.GetNode<Area2D>("Area2D");
            list.Add(area2D);
        }
        if (list.Contains(area)) {
            Area2D castedArea = (Area2D)area;
            var parent = castedArea.GetParent();
            var debris = parent.GetNode<Debris>("Debris");

            if (Mathf.Sign(debris.speed) < 0) {
                Node2D node = (Node2D)debris.GetParent();
                node.GlobalPosition = new Vector2(5000, debris.GlobalPosition.y);
            }
        }
    }
    
    private void reachEndAreaRight(object area) {
        
        var list = new ArrayList(); 
        var listOfDebris = GetTree().GetNodesInGroup("Debris");
        foreach (var obj in listOfDebris) {
            Node2D debris = (Node2D)obj;
            var area2D = debris.GetNode<Area2D>("Area2D");
            list.Add(area2D);
        }
        if (list.Contains(area)) {
            Area2D castedArea = (Area2D)area;
            var parent = castedArea.GetParent();
            var debris = parent.GetNode<Debris>("Debris");

            if (Mathf.Sign(debris.speed) >= 0) {
                Node2D node = (Node2D)debris.GetParent();
                node.GlobalPosition = new Vector2(-500, debris.GlobalPosition.y);
            }
        }
    }


    void loadNextScene() {
        gameState.StartingTimeInMs += Time.GetTicksMsec() - StartTime;

        if ("Earth".Equals(sceneName)) {
            GetTree().ChangeScene("res://startSpaceShip/StartSpaceShipOrbitScene.tscn");
        }

        if ("Orbit".Equals(sceneName)) {
            GetTree().ChangeScene("res://docking/DockingScene.tscn");
        }
    }

    private void stopCollision() {
        enableCollision = false; 
    }

    private void changeColorRed() {
        GetNode<Area2D>("Target").GetNode<Sprite>("Red").Visible = true;
        GetNode<Area2D>("Target").GetNode<Sprite>("Blue").Visible = false;
    }
    
    private void changeColorBlue() {
        GetNode<Area2D>("Target").GetNode<Sprite>("Red").Visible = false;
        GetNode<Area2D>("Target").GetNode<Sprite>("Blue").Visible = true;
    }
    
}
