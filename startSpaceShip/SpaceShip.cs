using Godot;
using System;

public class SpaceShip : Area2D {


    private bool moveSpaceShip = false; 
    public bool enableCollision = false; 
    
    public override void _Ready()
    {
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta) {

      if (moveSpaceShip) {
          Position += Transform.y * -400 * delta;
      }
  }

  public void startSpaceShip() {
      moveSpaceShip = true;
      enableCollision = true; 
  }

}
