using Godot;
using System;

public class SpaceDebris : Area2D {
    [Export] public float speed;

    private bool stopMoving = false;


    public override void _Ready()
    {
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta) {
      if (!stopMoving) {
          Position += Transform.x * speed * delta; 
      }
  }

  public void stopAndSize() {
      stopMoving = true;
      
      var animation = GetNode<AnimationPlayer>("AnimationPlayer").GetAnimation("MoveAndSize");
      var idx = animation.FindTrack(".:position:x");
      var idy = animation.FindTrack(".:position:y");
      var scaleX = animation.FindTrack(".:scale:x");
      var scaleY = animation.FindTrack(".:scale:y");

      int xValue;

      if (GlobalPosition.x > 960) {
          xValue = 300; 
      } else {
          xValue = -300;
      }
      
      
      animation.BezierTrackSetKeyValue(idx,0,GlobalPosition.x);
      animation.BezierTrackSetKeyValue(idx,1,GlobalPosition.x + xValue);
      animation.TrackSetKeyTime(idx, 1, 1);
      
      animation.BezierTrackSetKeyValue(idy,0,GlobalPosition.y);
      animation.BezierTrackSetKeyValue(idy,1,GlobalPosition.y + 1500);
      animation.TrackSetKeyTime(idy, 1, 1);
      
      animation.BezierTrackSetKeyValue(scaleX,0, 1);
      animation.BezierTrackSetKeyValue(scaleX,1,2);
      animation.TrackSetKeyTime(scaleX, 1, 1);
      
      animation.BezierTrackSetKeyValue(scaleY,0,1);
      animation.BezierTrackSetKeyValue(scaleY,1,2);
      animation.TrackSetKeyTime(scaleY, 1, 1);


      //GD.Print("StartPosition: ");
      //GD.Print(this.GlobalPosition.x);
      GetNode<AnimationPlayer>("AnimationPlayer").Play("MoveAndSize");
      
  }
  
  private void _on_AnimationPlayer_animation_finished(String anim_name)
  {
      if ("MoveAndSize".Equals(anim_name)) {
          //GD.Print("EndPosition: ");
          //GD.Print(this.GlobalPosition.x);
      }
  }
  
}



