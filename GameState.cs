using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class GameState : Node
{

    [Export]
    public readonly float PointMultiplierDeliveredPackages = 200;

    [Export]
    public readonly float PointMultiplierLostPackages = 50;
    [Export]
    public readonly float PointMultiplierFuel = 10;
    [Export]
    public readonly float PointMultiplierHealth = 500;
    [Export]
    public readonly float PointMultiplierTime = 2;
    [Export]
    public readonly float ShipHealthMax = 10;
    [Export]
    public readonly float ShipFuelMax = 100;
    [Export]
    public readonly float ShipFuelPerCargo = 10;
    [Export]
    public readonly float ShipRepairPointsPerCargo = 1;
    [Export]
    public readonly float ShipRepairPointsPerHitPoint = 3;
    

    public readonly List<Cargo> AllCargo = new List<Cargo>();

    [Export]
    public ulong CargoLoadingTimeInMs = 0; // Time spend during loading the Cargo on the Ship
    [Export]
    public ulong StartingTimeInMs = 0;     // Time spend during Launching the Ship to Space
    [Export]
    public ulong DeliveringTimeInMs = 0;   // Time spend delivering, to reach the target destination

    public long OverallScore = 0;
    // ShipFuel spend during delivery
    public float ShipFuelSpend = 0;
    // current ShipFuel
    public float ShipFuel = 0;
    // current ShipHealth
    public uint ShipHealth = 10;

    // current ShipRepairPoints
    public uint ShipRepairPoints = 0;
    public int DeliverdCargo => AllCargo.Count(cargo => cargo.CargoState == CargoState.deliverdSuccessfully);
    public int LoadedCargo => AllCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip);


    // how much cargo? must be max 16, because docking can't show more
    const int MAX_CARGO = 16;

    public bool FirstRound= true;

    public bool collideWithDebrisLevel2 = false;

    private bool gamePaused = false;
    public override void _Ready()
    {
        GD.Randomize();

        loadRandomCargo(MAX_CARGO);
    }

    public long Points()
    {

        int lost_in_loading = AllCargo.Count(cargo => cargo.CargoState == CargoState.inMissedArea);
        int lost_in_start = AllCargo.Count(cargo => cargo.CargoState == CargoState.lostDuringStart);
        int lost_in_transit = AllCargo.Count(cargo => cargo.CargoState == CargoState.lostDuringTransport);
        int lost_cargo = lost_in_transit + lost_in_start + lost_in_loading;

        long points_deliverytime = (long) ((CargoLoadingTimeInMs + StartingTimeInMs + DeliveringTimeInMs) / 1000 * -PointMultiplierTime);
        long points_deliverd_cargo = (long) ((long)DeliverdCargo * PointMultiplierDeliveredPackages);
        long points_lost_cargo = (long) ((long)lost_cargo * -PointMultiplierLostPackages);
        long points_fuel = (long) ( ShipFuelSpend  * -PointMultiplierFuel ) ;
        long points_health = (long)( (  ShipHealthMax - ShipHealth ) * -PointMultiplierHealth);

        GD.Print("time:", points_deliverytime,"cargo:",points_deliverd_cargo,"fuel:",points_fuel,"health:",points_health);

        return points_deliverd_cargo + points_fuel + points_health + points_deliverytime;
    }

    private void loadRandomCargo(uint count)
    {

        uint max = (uint)CargoType.MaxCargoType;

        for (int i = 0; i < count; i++)
        {
            var random_cargo = (int)(GD.Randi() % max);
            AllCargo.Add(LoadCargoScene((CargoType)random_cargo));
        }
    }


    private static Cargo LoadCargoScene(CargoType cargoType)
    {
        var cargoTypeName = Enum.GetName(typeof(CargoType), cargoType);
        var packedScene = (PackedScene)GD.Load($"res://cargo/{cargoTypeName}.tscn");
        var cargo = packedScene.Instance<Cargo>();
        cargo.Visible = false;
        return cargo;
    }

    public void SetGamePaused(bool pauseValue)
    {
        gamePaused = pauseValue;
        UpdatePaused();
    }

    public bool GetGamePaused()
    {
        return gamePaused;
    }

    private void UpdatePaused()
    {
        GetTree().Paused = gamePaused;
    }

    public override void _Input(InputEvent @event)
    {
        if (@event.IsActionPressed("trigger_dev_mode"))
        {
            GetTree().ChangeScene("res://DevModeScene.tscn");
        }
    }

    public void UpdateDeliverdCargo(Cargo cargo) {
        cargo.CargoState = CargoState.deliverdSuccessfully;

        ShipRepairPoints += (uint)(1 * ShipRepairPointsPerCargo);
        ShipFuel +=  Mathf.Clamp((1 * ShipFuelPerCargo),0,ShipFuelMax);

        // Todo calculation new hitpoints by converting repair points to health points
    }
    
    public void Reset()
    {
        // cleanup if game is started again   
        CargoLoadingTimeInMs = 0;
        DeliveringTimeInMs = 0;
        StartingTimeInMs = 0;
        OverallScore += Points();
        AllCargo.Clear();
        loadRandomCargo(MAX_CARGO);
    }
}
