using System.Diagnostics;
using Godot;
using Godot.Collections;
using File = Godot.File;

public class UserSettings : Node
{
    private Timer SaveTimer => GetNode<Timer>("SaveTimer");

    public static float MasterVolume
    {
        get => (float)GetValue(nameof(MasterVolume));
        set => SetValue(nameof(MasterVolume), value);
    }

    public static float MusicVolume
    {
        get => (float)GetValue(nameof(MusicVolume));
        set => SetValue(nameof(MusicVolume), value);
    }

    public static float SFXVolume
    {
        get => (float)GetValue(nameof(SFXVolume));
        set => SetValue(nameof(SFXVolume), value);
    }

    private static readonly Dictionary DefaultValues = new Dictionary
    {
        { nameof(MasterVolume), -15.0f },
        { nameof(MusicVolume), -10.0f },
        { nameof(SFXVolume), 0.0f },
    };

    private const string FileName = "user://settings.json";
    private static UserSettings _instance;

    private Dictionary _values = DefaultValues.Duplicate();

    public override void _Ready()
    {
        Debug.Assert(_instance == null);
        _instance = this;
        Load();
    }

    private void Load()
    {
        var file = new File();
        if (!file.FileExists(FileName))
        {
            return;
        }

        file.Open(FileName, File.ModeFlags.Read);
        var fileContent = file.GetLine();
        if (fileContent.Length < 1)
        {
            return;
        }

        GD.Print($"Loading {nameof(UserSettings)}: {fileContent}");
        var result = JSON.Parse(fileContent).Result;
        _values = (Dictionary)result;
        file.Close();
    }

    private void Save()
    {
        var file = new File();
        file.Open(FileName, File.ModeFlags.Write);
        var fileContent = JSON.Print(_values);
        GD.Print($"Saving {nameof(UserSettings)}: {fileContent}");
        file.StoreLine(fileContent);
        file.Close();
    }

    public static object GetValue(string key)
    {
        if (_instance._values.Contains(key))
        {
            return _instance._values[key];
        }

        return DefaultValues[key];
    }

    public static void SetValue(string key, object value)
    {
        _instance._values[key] = value;
        _instance.SaveTimer.Start();
    }

    public static void Delete()
    {
        var file = new File();
        if (file.FileExists(FileName))
        {
            var dir = new Directory();
            dir.Remove(FileName);
        }

        _instance.SaveTimer.Stop();
        _instance._values = DefaultValues.Duplicate();
        _instance.Save();
    }
}
