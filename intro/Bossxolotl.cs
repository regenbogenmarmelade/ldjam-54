using Godot;
using System;
using System.Collections.Generic;

public class Bossxolotl : ColorRect
{
    public AnimationPlayer AnimationPlayer => GetNode<AnimationPlayer>("AnimationPlayer");
    public Node2D SpeechBubble => GetNode<Node2D>("SpeechBubble");
    public Button StartButton => GetNode<Button>("StartButton");
    public AudioStreamPlayer2D AxolotlStepsSound => GetNode<AudioStreamPlayer2D>("AxolotlStepsSound");
    public AudioStreamPlayer2D BossxolotlSpeechSound => GetNode<AudioStreamPlayer2D>("BossxolotlSpeechSound");
    
    public override void _Ready()
    {
        SpeechBubble.Hide();
        StartButton.Hide();
    }

    public void StartWalking()
    {
        AnimationPlayer.Play("Walk");
    }

    private void StopWalking()
    {
        AxolotlStepsSound.Stop();
        BossxolotlSpeechSound.Play();
        SpeechBubble.Show();
        StartButton.Show();
    }
    
    private void StartNextScene()
    {
        GetTree().ChangeScene("res://cargohold/Cargohold.tscn");
    }

    private void OnStepLoopIteration()
    {
        GD.Print("OnStepLoopIteration");
    }
}
