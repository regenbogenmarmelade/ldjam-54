using Godot;
using System;

public class Intro : Node2D
{
    public Dialog Dialog => GetNode<Dialog>("Dialog");
    
    public override void _Ready()
    {
        Dialog.Show();
    }
}
