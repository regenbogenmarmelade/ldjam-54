using Godot;
using System;

public class VolumeSlider : Label
{
    [Export] public AudioBusName AudioBusName;

    private int AudioBusIndex;

    private HSlider HSlider => GetNode<HSlider>("HSlider");
    private AudioStreamPlayer2D ButtonSound => GetNode<AudioStreamPlayer2D>("ButtonSound");
    
    public override void _Ready()
    {
        AudioBusIndex = AudioServer.GetBusIndex(Enum.GetName(typeof(AudioBusName), AudioBusName));

        HSlider.Value = LoadValueFromSettings();
        SetBusVolume((float)HSlider.Value);

        HSlider.Connect("value_changed", this, nameof(OnValueChanged));
    }

    private double LoadValueFromSettings()
    {
        switch (AudioBusName)
        {
            case AudioBusName.Master: return UserSettings.MasterVolume;
            case AudioBusName.Music: return UserSettings.MusicVolume;
            case AudioBusName.SFX: return UserSettings.SFXVolume;
            default:
                throw new NotImplementedException(Enum.GetName(typeof(AudioBusName), AudioBusName));
        }
    }

    private void SaveValueToSettings()
    {
        switch (AudioBusName)
        {
            case AudioBusName.Master:
                UserSettings.MasterVolume = (float)HSlider.Value;
                break;
            case AudioBusName.Music:
                UserSettings.MusicVolume = (float)HSlider.Value;
                break;
            case AudioBusName.SFX:
                UserSettings.SFXVolume = (float)HSlider.Value;
                break;
            default:
                throw new NotImplementedException(Enum.GetName(typeof(AudioBusName), AudioBusName));
        }
    }

    private void OnValueChanged(float value)
    {
        SetBusVolume(value);
        
        if (!ButtonSound.Playing) {
            ButtonSound.Play();
        }
    }

    private void SetBusVolume(float value)
    {
        AudioServer.SetBusVolumeDb(AudioBusIndex, value);
        AudioServer.SetBusMute(AudioBusIndex, value <= HSlider.MinValue + HSlider.Step);
    }

    private void OnDragEnded(bool value_changed)
    {
        if (value_changed) {
            SaveValueToSettings();
        }
    }
}
