using Godot;

public class Dialog : Label
{
    [Signal]
    public delegate void DialogClosed();

    [Export] public bool PauseGame;

    [Export] public bool AlreadyShown;

    private Timer Timer => GetNode<Timer>("Timer");
    public override void _Ready()
    {
        Hide();
    }

    private void CloseDialog()
    {
        Hide();

        if (PauseGame)
        {
            GetTree().Paused = false;
        }
        EmitSignal(nameof(DialogClosed));
    }

    public void ShowOnce(float waitTimeInSeconds = float.NaN)
    {
        if (AlreadyShown) {
            return;
        }

        AlreadyShown = true;

        if (float.IsNaN(waitTimeInSeconds))
        {
            OnTimeout();
        }
        else
        {
            Timer.WaitTime = waitTimeInSeconds;
            Timer.Start();
        }
    }

    private void OnTimeout()
    {
        Show();

        if (PauseGame)
        {
            GetTree().Paused = true;
        }
    }

    public void CancelDelayedShow()
    {
        Timer.Stop();
    }
}
