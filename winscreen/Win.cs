using Godot;
using System;
using System.Linq;

public class Win : Node2D
{
    private GameState gameState => GetParent().GetNode<GameState>("/root/GameState");
    private Label Success => GetNode<Label>("Label Success");
    private Label NotHappy => GetNode<Label>("Label NotHappy");
    private Label Delivered => GetNode<Label>("Text Delivered");
    private Label Lost => GetNode<Label>("Text Lost");
    private Label LostInLoading => GetNode<Label>("Text LostInLoading");
    private Label LostInStart => GetNode<Label>("Text LostInStart");
    private Label LostInTransit => GetNode<Label>("Text LostInTransit");
    private Label DeliveryTime => GetNode<Label>("Text DeliveryTime");
    private Label Money => GetNode<Label>("Text Money");

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        int delivered = gameState.AllCargo.Count(cargo => cargo.CargoState == CargoState.deliverdSuccessfully);
        int lost_in_loading = gameState.AllCargo.Count(cargo => cargo.CargoState == CargoState.inMissedArea);
        int lost_in_start = gameState.AllCargo.Count(cargo => cargo.CargoState == CargoState.lostDuringStart);
        int lost_in_transit = gameState.AllCargo.Count(cargo => cargo.CargoState == CargoState.lostDuringTransport);
        int lost = lost_in_transit + lost_in_start + lost_in_loading;
        float fuel = gameState.ShipFuel;

        var fail = DoMessages(delivered, fuel, lost);

        Delivered.Text = fail ? "zilch" : delivered.ToString();
        Lost.Text = fail ? "all of it" : lost.ToString();
        LostInLoading.Text = fail ? "don't care" : lost_in_loading.ToString();
        LostInStart.Text = fail ? "hggnnn" : lost_in_start.ToString();
        LostInTransit.Text = fail ? "whatever" : lost_in_transit.ToString();

        // this is garbage. need to fix
        int time = (int)(gameState.DeliveringTimeInMs + gameState.CargoLoadingTimeInMs + gameState.StartingTimeInMs) / 1000;
        int sec = time % 60;
        int min = time / 60 % 60;
        int hrs = time / 60 / 60;

        DeliveryTime.Text =
            hrs > 0
                ? string.Format("{0}:{1}:{2}", hrs.ToString().PadLeft(2, '0'), min.ToString().PadLeft(2, '0'), sec.ToString().PadLeft(2, '0'))
                : string.Format("{0}:{1}", min.ToString().PadLeft(2, '0'), sec.ToString().PadLeft(2, '0'));

        // complicated money calculation algorithm! do not touch with gefinerpoken unless you understand advanced axolotlnomics!
        var money = gameState.Points();
        var overallMoney = gameState.OverallScore + money;
        Money.Text = "$" + overallMoney + " (+" + money + ")";
    }

    private bool DoMessages(int delivered, float fuel, int lost)
    {
        if (lost == 0 && fuel > 0)
        {
            Success.Text = "Delivery Full Success!";
            NotHappy.Text = "Bossxolotl is... oddly less not happy than usual! Good job!?";
        }

        if (delivered < 1 || fuel < 1)
            Success.Text = "Delivery Failed!";

        if (fuel < 1)
            NotHappy.Text = "You managed to run out of fuel! Do you think ship fuel grows on trees?";

        if (delivered < 1)
            NotHappy.Text = "You managed to lose all cargo! Bossxolotl is furious!";

        return delivered < 1 || fuel < 1;
    }


    private void OnNewGame()
    {
        gameState.Reset();
        GetTree().ChangeScene("res://cargohold/Cargohold.tscn");
    }


}

