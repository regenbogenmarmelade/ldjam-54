using Godot;
using System;

public class ClickableObject : Node2D {



    [Export] private String lableText = "";

    private int countClicks = 0; 
    
    private Label infoText => GetNode<Label>("InfoText");
    private Sprite sprite => GetNode<Sprite>("Sprite");


    public override void _Ready() {
        infoText.Text = lableText; 
        infoText.Visible = false;
        GetNode<Sprite>("Speechbubble").Visible = false;
    }

    
    private void ShowInfoText() {
        infoText.Visible = true;
        sprite.Modulate = new Color(0.5f, 1, 1, 1); 
    }
    
    private void HideInfoText() {
        infoText.Visible = false; 
        sprite.Modulate = new Color(1, 1, 1, 1); 
    }

    private void ShowReaction(Node n, InputEvent @event, int idx) {

        if (countClicks > 3) {
            GetNode<Sprite>("Speechbubble").GetNode<Label>("Label").Text = "Hey! Please stop!"; 
        }
        
        if (@event is InputEventMouseButton && @event.IsPressed()) {
            GetNode<Sprite>("Speechbubble").Visible = true;
            GetNode<Timer>("Timer").Start();
            countClicks = countClicks +1; 
        }
    }

    private void OnTimerTimeout() {
        GetNode<Sprite>("Speechbubble").Visible = false;
    }
}
