using Godot;
using System;

public class SceneMenuButton : MenuButton
{

    private MenuButton mButton;
    
    public override void _Ready() {
        mButton = GetNode<MenuButton>("%SceneMenuButton");
        PopupMenu popupMenu = mButton.GetPopup();
        popupMenu.AddItem("Cargo-hold", 0);
        popupMenu.AddItem("Start-ramp", 1);
        popupMenu.AddItem("Docking-station", 2);

        popupMenu.Connect("id_pressed", this, nameof(HandleItemPressed));
    }

    private void HandleItemPressed(int index) {
        switch (index) {
            case 0:
                GetTree().ChangeScene("res://cargohold/Cargohold.tscn");
                break;
            case 1:
                GetTree().ChangeScene("res://startSpaceShip/MiniGameStartSpaceShip.tscn");
                break;
            case 2:
                GetTree().ChangeScene("res://docking/DockingScene.tscn");
                break;
        }
    }
    
}
