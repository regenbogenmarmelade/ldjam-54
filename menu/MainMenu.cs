using Godot;
using System;

public class MainMenu : Control {
    private void StartIntro() {
        GetTree().ChangeScene("res://intro/Intro.tscn");
    }

    private void OnExitButton()
    {
        GetTree().Quit();
    }
}
