using Godot;
using System;

public class PauseMenu : Control
{
    private GameState gameState;
    public override void _Ready() {
        gameState = GetNode<GameState>("/root/GameState");
    }
    
    public override void _UnhandledInput(InputEvent @event) {
        if (@event.IsActionPressed("pause")) {
            gameState.SetGamePaused(!gameState.GetGamePaused());
            ToggleVisible();
        } else {
            base._UnhandledInput(@event);
        }
    }

    private void ToggleVisible() {
        this.Visible = !this.Visible;
    }
    
    private void OnResumeButton() {
        gameState.SetGamePaused(false);
        ToggleVisible();
    }
    
    private void OnMainMenuButton() {
        gameState.SetGamePaused(false);
        GetTree().ChangeScene("res://menu/MainMenu.tscn");
    }
    private void OnExitButton() {
        GetTree().Quit();
    }
}
