using Godot;
using System;

public class Asteroid1 : KinematicBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    
    [Export]
    public float RotationSpeed;
    [Export]
    public Vector2 Speed;

    [Export] public String text; 
    [Export] public bool isSpriteVisible = true; 
    
    const float rndRotation = 0.002f;
    const float rndSpeed = 0.08f;
    
    private Random rnd;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        rnd = new Random();
        GetNode<Label>("Label").Visible = false; 
        GetNode<Label>("Label").Text = text; 
        
        RotationSpeed = (float)rnd.NextDouble() * rndRotation - rndRotation/2;
        Speed         = new Vector2((float)rnd.NextDouble() * rndSpeed, (float)rnd.NextDouble() * rndSpeed);

        GetNode<Sprite>("Sprite").Visible = isSpriteVisible; 
    }
    

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        Transform = Transform.Rotated(RotationSpeed * delta * 144).Translated(Speed * delta * 144);
    }

    public void showText() {
        GetNode<Label>("Label").Visible = true;
        GetNode<Timer>("Timer").Start();
    }

    public void hideText() {
        GetNode<Label>("Label").Visible = false; 
    }
}
