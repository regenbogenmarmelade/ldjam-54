using Godot;
using System;
using System.Linq;
using System.Collections.Generic;

public class Ship : KinematicBody2D
{
    [Export]
    public float RotationSpeed;
    private float LastRotationSpeed;
    [Export]
    public Vector2 In;
    [Export]
    public Vector2 Speed;
    [Export]
    public float Fuel;
    // [Export]
    // public int Cargo;
    [Export]
    public int MaxCargo;
    [Export]
    public ulong StartTime;
    
    [Export]
    public List<Cargo> LoadedCargo;
    
    [Export]
    public bool RotationDecay = true;
    
    const float DELTA_ROT = 0.0216f;
    const float DELTA_SPEED = 60f;
    
    const float MAX_SPEED = 300f;
    const float MAX_ROTATION = 0.01f;
    const float BOUNCE_DAMPENING = 0.5f;
    const float FUEL_PER_TICK = 0.005f;
    const float ROTATION_DECAY_TIME = 1.5f; // in seconds
    
    const float LOW_COLLISION_SPEED = 100f;
    const float MEDIUM_COLLISION_SPEED = 200f;
    
    const float COLLISION_FUEL_COST = 2f;
    
    private AudioStreamPlayer2D BoostSound => GetNode<AudioStreamPlayer2D>("BoostSound");
    private AudioStreamPlayer2D ThrusterLoopSound => GetNode<AudioStreamPlayer2D>("ThrusterLoopSound");
    private AudioStreamPlayer2D CrashSound => GetNode<AudioStreamPlayer2D>("CrashSound");
    
    private Sprite FireBack1      => GetNode<Sprite>("Fire/FireBack1");
    private Sprite FireBack2      => GetNode<Sprite>("Fire/FireBack2");
    private Sprite FireFrontRight => GetNode<Sprite>("Fire/FireFrontRight");
    private Sprite FireFrontLeft  => GetNode<Sprite>("Fire/FireFrontLeft");
    private Sprite FireBackRight  => GetNode<Sprite>("Fire/FireBackRight");
    private Sprite FireBackLeft   => GetNode<Sprite>("Fire/FireBackLeft");
    
    private ProgressBar HUDFuelBar           => GetParent().GetNode<ProgressBar>("HUD/FuelBar");
    private RichTextLabel HUDContainerCount  => GetParent().GetNode<RichTextLabel>("HUD/ContainerCount");
    private Label HUDTimer                   => GetParent().GetNode<Label>("HUD/Timer");
    private Sprite HUDTargetIndicator        => GetParent().GetNode<Sprite>("HUD/TargetIndicator");
    private Node2D Target                    => GetParent().GetNode<Node2D>("Target");
    
    private GameState gameState => GetParent().GetNode<GameState>("/root/GameState");

    private int visibleContainer = 15; 
    
    public override void _Ready()
    {
        RotationSpeed = 0f;
        Speed = new Vector2(0,0);
        Fuel = 100f;
        StartTime = Time.GetTicksMsec();
        
        // get cargo from earlier scenes
        LoadedCargo = gameState.AllCargo;
        
        // if someone started the scene from dev menu, just get all cargo and flag them as loaded
        if (LoadedCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip) == 0) 
        {
            foreach (var cargo in LoadedCargo)
            {
                cargo.CargoState = CargoState.isLoadedOnCargoShip;
            };
        }
        
        MaxCargo = LoadedCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip);
        while (visibleContainer > MaxCargo)
        {
            hideContainer();    
        }
    }
    
    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        GetNode<Camera2D>("Camera2D").GetNode<Sprite>("Hands").Rotate(-0.02f);
        
        bool accel        = Input.IsActionPressed("dock_accel");
        bool turn_left    = Input.IsActionPressed("dock_rot_left");
        bool turn_right   = Input.IsActionPressed("dock_rot_right");
        bool strafe_left  = Input.IsActionPressed("dock_strafe_left");
        bool strafe_right = Input.IsActionPressed("dock_strafe_right");
        
        In = new Vector2(0,0);
        
        foreach (Sprite s in GetNode<Node2D>("Fire").GetChildren()) 
        {
            s.Hide();
        }

        if (accel || turn_left || turn_right || strafe_left || strafe_right)
        {
            StartThrusterSound();
        }
        else
        {
            ThrusterLoopSound.Stop();
        }
     
        if (turn_left)
        {
            RotationSpeed -= delta * DELTA_ROT;
            LastRotationSpeed = RotationSpeed;
            FireFrontRight.Show();
            FireBackLeft.Show();
            
            Fuel -= FUEL_PER_TICK;
        }
        if (turn_right)
        {
            RotationSpeed += delta * DELTA_ROT;
            LastRotationSpeed = RotationSpeed;
            FireFrontLeft.Show();
            FireBackRight.Show();
            
            Fuel -= FUEL_PER_TICK;
        }
        if (accel)
        {
            In.y -= delta * 2 * DELTA_SPEED;
            FireBack1.Show();
            FireBack2.Show();
            
            Fuel -= 2*FUEL_PER_TICK;
        }
        if (strafe_left)
        {
            In.x -= delta * DELTA_SPEED;
            FireFrontRight.Show();
            FireBackRight.Show();
            
            Fuel -= FUEL_PER_TICK;
        }
        if (strafe_right)
        {
            In.x += delta * DELTA_SPEED;
            FireFrontLeft.Show();
            FireBackLeft.Show();
            
            Fuel -= FUEL_PER_TICK;
        }
    
        Speed = Speed.LimitLength(MAX_SPEED);
        RotationSpeed = Mathf.Clamp(RotationSpeed, -MAX_ROTATION, MAX_ROTATION);
        
        Speed += In.Rotated(Rotation);
        Rotation += RotationSpeed;
        
        var collision = MoveAndCollide(delta * Speed.Rotated(delta * RotationSpeed));
        if (collision != null) 
        {
            HandleCollisions(collision);
        }
        
        if (Fuel <= 0 ) 
        {
            Fail("You ran out of fuel!");
        } else if (LoadedCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip) <= 0) 
        {
            Fail("You lost all your cargo!");
        }
        
        if (RotationDecay && !turn_right && !turn_left && RotationSpeed != 0) {
            RotationSpeed -= LastRotationSpeed / ROTATION_DECAY_TIME * delta;
            if (RotationSpeed * LastRotationSpeed < 0)
                RotationSpeed = 0;
        }
    
        updateHUD();

    }
    
    private void updateHUD()
    {
        HUDFuelBar.Value = Fuel;
        HUDContainerCount.Text = LoadedCargo.Count(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip) + "/" + LoadedCargo.Count;
        
        var time = (Time.GetTicksMsec() - StartTime) / 1000;
        
        var sec = ((int)time) % 60;
        var min = ((int)time) / 60 % 60;
        var hrs = ((int)time) / 60 / 60;
        HUDTimer.Text =
            hrs > 0
                ? string.Format("{0}:{1}:{2}", hrs.ToString().PadLeft(2,'0'), min.ToString().PadLeft(2,'0'), sec.ToString().PadLeft(2,'0'))
                : string.Format("{0}:{1}", min.ToString().PadLeft(2,'0'), sec.ToString().PadLeft(2,'0'));

        // target indicator is rotated by 90° because the sprite is facing down
        HUDTargetIndicator.Rotation = (Target.Position - Position).Angle() - Mathf.Pi/2;
    }
    
    void HandleCollisions(KinematicCollision2D collision)
    {
        var other = collision.Collider;
        switch (other) 
        {
            case Asteroid1 a:
                float relativeSpeed = (Speed - collision.ColliderVelocity).Length();
                if (relativeSpeed < LOW_COLLISION_SPEED)
                {
                    Fuel -= COLLISION_FUEL_COST;
                }
                else
                if (relativeSpeed < MEDIUM_COLLISION_SPEED)
                {
                    Fuel -= COLLISION_FUEL_COST;
                    loseContainer(1);
                }
                else 
                {
                    Fuel -= 2 * COLLISION_FUEL_COST;
                    //yep twice
                    loseContainer(2);
                }
                
               
                // lets assume that the asteroid is much heavier than our spaceship and that they're mostly round.
                // that simplifies collision because we don't need to do full rotation/collision
                // for mass asteroid >> mass spaceship, the combined center of mass is very close to the center of the asteroid
                // and since it's circular, the angular momentum conveyed from that to the spaceship in minimal.
                // so in effect the spaceship is hitting a rigicbody at a weird angle and the collision is not fully bouncy
                // this means:
                //  - velocity of the spaceship is bounced with a fixed dampening factor
                //  - rotational speed of the spaceship gets the perpendicular component of the collision normal
                Speed = Speed.Bounce(collision.Normal) * BOUNCE_DAMPENING;
                RotationSpeed = RotationSpeed * collision.Normal.Dot((collision.Position - GlobalPosition).Normalized());
                LastRotationSpeed = RotationSpeed;
                
                break;
            default:
                GD.Print("got unknown collision!");
                break;
        }
    }

    private void StartThrusterSound()
    {
        if (ThrusterLoopSound.Playing)
        {
            return;
        } 
        BoostSound.Play();
        ThrusterLoopSound.Play();
    }

    private void OnDockingAreaEntered(object body)
    {
        switch (body)
        {
            case Ship s:
                Win();
                break;
            default:
                break;
        }
    }

    /* TODOD:
        - more start conditions
        - landing conditions
        - MAYBE also linear dampening, but less strong than angular dampening
    */

    private void handleRadar(object body) {
        //GD.Print(body);
        var k = (KinematicBody2D)body;
        //GD.Print(k.Name);

        if (!"Ship".Equals(k.Name)) {
            var asteroid = (Asteroid1)body;
            asteroid.showText();
        }
    }
    
    private void hideContainer() {
        if (visibleContainer >= 0) {
            var children = GetNode<Node2D>("Containers").GetChildren();
            var child = children[15-visibleContainer];
            var sprite = (Sprite)child;
            sprite.Visible = false;
            visibleContainer--; 
        }
    }

    private void loseContainer(int n) {
        for (int i = 0; i < n; i++) 
        {
            hideContainer();
            var ci = LoadedCargo.FindIndex(cargo => cargo.CargoState == CargoState.isLoadedOnCargoShip);
            if (ci > -1)
            { 
                LoadedCargo[ci].CargoState = CargoState.lostDuringTransport;
            }
        }

        switch(n)
        {
            case 1:
                GetNode<AnimationPlayer>("AnimationPlayer").Play("LoseOneCargo");
                break;
            case 2:
                GetNode<AnimationPlayer>("AnimationPlayer").Play("LoseTwoCargos");
                break;
            default:
                GD.Print("don't know how to animate 3 lost cargo!!");
                break;
        }      

        if (!CrashSound.Playing)
        {
            CrashSound.Play();
        }
    }
    
    public void Fail(string reason){
        // ?
        GD.Print("Fail");
        foreach (Cargo cargo in gameState.AllCargo)
        {
            if ( cargo.CargoState == CargoState.isLoadedOnCargoShip ) {
                cargo.CargoState = CargoState.lostDuringTransport;
            } 
        }

        Win();
    }
    
    public void Win() 
    {
        foreach (Cargo cargo in gameState.AllCargo)
        {
            if ( cargo.CargoState == CargoState.isLoadedOnCargoShip ) {
                cargo.CargoState = CargoState.deliverdSuccessfully;
            } 
        }
        //write state back to Gamestate
        var end_time = Time.GetTicksMsec();
        gameState.DeliveringTimeInMs = end_time - StartTime;
        gameState.ShipFuelSpend = 100 - Fuel;
        gameState.ShipFuel = Fuel;
        
        GetTree().ChangeScene("res://winscreen/win.tscn");
    }
}
